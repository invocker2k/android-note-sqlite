package com.example.save_session_with_sql;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int VIEW_NOTE = 9999;
    private static final int DELETE_NOTE = 9998;
    private static final int UPDATE_NOTE = 9997;
    private static final int CREATE_NOTE = 9996;

    private ListView lvNote;
    private FloatingActionButton addNote;
    private int CODE = 11;
    MydatabaseHeper myDB;
    ArrayList<Note> notes;
    ArrayAdapter<Note> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvNote = findViewById(R.id.lv_note);
        addNote = findViewById(R.id.add_Note);

        myDB = new MydatabaseHeper(MainActivity.this);
        notes = myDB.getAllNote();

        // create adapter
         adapter = new Myadapter();
        lvNote.setAdapter(adapter);
        registerForContextMenu(lvNote);
        addNote.setOnClickListener(v -> {

                Intent intent = new Intent(getApplicationContext(), AddNote.class);
                startActivityForResult(intent, CODE);

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListNotes();
    }

    private void updateListNotes(){
        notes = myDB.getAllNote();
        adapter = new Myadapter();
        lvNote.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Chose an action");

        menu.add(0, VIEW_NOTE,0, "View Note");
        menu.add(0, CREATE_NOTE,1, "Create Note");
        menu.add(0, UPDATE_NOTE,2, "Update Note");
        menu.add(0, DELETE_NOTE,3, "Delete Note");

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Note selectNote = (Note) lvNote.getItemAtPosition(info.position);
        switch (item.getItemId())
        {
            case (VIEW_NOTE):
                Toast.makeText(getApplicationContext(), selectNote.getContent(), Toast.LENGTH_LONG).show();
                break;
            case(CREATE_NOTE) :
                break;
            case (DELETE_NOTE):
               new AlertDialog.Builder(this)
                       .setMessage(selectNote.getTitle() + "you are sure delete note?")
                       .setCancelable(false)
                       .setPositiveButton("Yes",(dialog, which) -> {
                               deleteNote(selectNote);
                       })
                       .setNegativeButton("No",null)
                       .show();

                break;
            case(UPDATE_NOTE) :
                break;
            default: return false;
        }
        return true;
    }

    private void deleteNote(Note note){
        MydatabaseHeper db = new MydatabaseHeper(this);
        db.deleteNote(note);
        this.notes.remove(note);
        this.adapter.notifyDataSetChanged();
    }
    private class Myadapter extends ArrayAdapter<Note> {

        public Myadapter() {
            super(MainActivity.this, R.layout.item_note,notes);
        }
        @Override
        public int getCount() {
            return super.getCount();
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Note note = notes.get(position);

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_note, null);
            TextView title = convertView.findViewById(R.id.txt_title);
            TextView content = convertView.findViewById(R.id.txt_content);
            title.setText(note.getTitle());
            content.setText(note.getContent());
            return convertView;

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE) {
            if(resultCode == Activity
                    .RESULT_OK){
                String title=data.getStringExtra("title");
                String content=data.getStringExtra("content");
//                Toast.makeText(getApplicationContext(),title,Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),content,Toast.LENGTH_LONG).show();

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(getApplicationContext(),"no",Toast.LENGTH_LONG).show();

            }
        }
    }
}