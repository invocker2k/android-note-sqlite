package com.example.save_session_with_sql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddNote extends AppCompatActivity {

    private EditText edtTitlelNote;
    private EditText edtContentNote;
    private Button saveNote;
    private Button cancelNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        init();

        saveNote.setOnClickListener((view) ->{
            String title = edtTitlelNote.getText().toString();
            String content = edtContentNote.getText().toString();

            MydatabaseHeper db = new MydatabaseHeper(AddNote.this);
            Note note = new Note(title, content);
            // save note
//            Toast.makeText(getApplicationContext(),"title" + note.getTitle() +"\n content:" + note.getContent(), Toast.LENGTH_LONG).show();
            Long res = db.addNote(note);
            Toast.makeText(getApplicationContext(),"row" + res, Toast.LENGTH_LONG).show();
            db.close();
            
            Intent returnIntent = new Intent();
            returnIntent.putExtra("title",title);
            returnIntent.putExtra("content",content);

            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        });
    }

    private void init() {
        edtTitlelNote = findViewById(R.id.title_note);
        edtContentNote = findViewById(R.id.content_note);
        saveNote = findViewById(R.id.save_note);
        cancelNote = findViewById(R.id.cancel_note);
    }
}