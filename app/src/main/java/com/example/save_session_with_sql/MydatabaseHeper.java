package com.example.save_session_with_sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MydatabaseHeper extends SQLiteOpenHelper {
    public static final String DB_NAME = "DB_OF_KING";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLE = "notes";
    public static final String COL_NOTE_ID = "id";
    public static final String COL_NOTE_TITLE = "title";
    public static final String COL_NOTE_CONTENT = "content";

    public MydatabaseHeper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + DB_TABLE + "("+COL_NOTE_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COL_NOTE_TITLE + " text NOT NULL,"
                + COL_NOTE_CONTENT + " text"
                + ")";
    db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP table if EXISTS " + DB_TABLE;
        db.execSQL(sql);
        onCreate(db);
    }
    // phuong thuc them 1 ghi chu moi

    public Long addNote(Note note){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues =  new ContentValues();
        contentValues.put(COL_NOTE_TITLE, note.getTitle());
        contentValues.put(COL_NOTE_CONTENT, note.getContent());
//        contentValues.put(COL_NOTE_ID, note.getId());

        Long res =  db.insert(DB_TABLE,null, contentValues);


        db.close();
        return res;
    }

    public ArrayList<Note> getAllNote(){
        String sql = "select * from " + DB_TABLE;
         ArrayList<Note> notes = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cu = db.rawQuery(sql,null);
        while (cu.moveToNext()){
          Note note = new Note(cu.getInt(0),cu.getString(1), cu.getString(2));
          notes.add(note);
        }
        db.close();
        return  notes;
    }

    public void deleteNote(Note note){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE,COL_NOTE_ID+ " = ?", new String[] {
                String.valueOf(note.getId())
        });
        db.close();
    }
}
